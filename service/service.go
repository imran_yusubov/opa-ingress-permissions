package service

import (
	"bytes"
	"github.com/ms-opa-permission/client"
	"github.com/ms-opa-permission/model"
	log "github.com/sirupsen/logrus"
)

// IOpaPermissionService ...
type IOpaPermissionService interface {
	Check([]byte) (*bytes.Buffer, error)
}

// OpaPermissionService ...
type OpaPermissionService struct {
	*client.OpaClient
}

// NewOpaPermissionService ...
func NewOpaPermissionService(opaClient *client.OpaClient) *OpaPermissionService {
	return &OpaPermissionService{opaClient}
}

// Check ...
func (ops *OpaPermissionService) Check(requestBody []byte) (*model.OpaResultResponse, error) {
	log.Info("Check.start")

	response, err := ops.OpaClient.Check(requestBody)
	if err != nil {
		log.Error("Check.error opaClient failed  ", err)
		return nil, err
	}

	log.Info("Check.success")
	return response, nil
}
