package model

// CheckPermissionRequest ...
type CheckPermissionRequest struct {
	Policy string
}

// OpaResultResponse ...
type OpaResultResponse struct {
	Result bool `json:"result"`
}
