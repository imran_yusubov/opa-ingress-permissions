package client

import (
	"bytes"
	"encoding/json"
	"github.com/ms-opa-permission/model"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

// IOpaClient ...
type IOpaClient interface {
	Check(requestBody []byte) (*model.OpaResultResponse, error)
}

// OpaClient ...
type OpaClient struct {
	OpaRootPath string
}

// Check ...
func (c *OpaClient) Check(requestBody []byte) (*model.OpaResultResponse, error) {
	var request model.CheckPermissionRequest

	err := json.Unmarshal(requestBody, &request)
	if err != nil {
		log.Error("Check.error can't unmarshall request ", err)
		return &model.OpaResultResponse{}, nil
	}

	resp, err := http.Post(c.OpaRootPath + "/v1/data/" + request.Policy,
		"application/json",
		bytes.NewBuffer(requestBody))
	if err != nil {
		log.Error("Check.error OPA client ", err)
		return &model.OpaResultResponse{}, nil
	}

	responseBody, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return &model.OpaResultResponse{}, nil
	}

	var response model.OpaResultResponse
	err = json.Unmarshal(responseBody, &response)
	if err != nil {
		log.Error("Check.error can't unmarshall OpaResultResponse ", err)
		return &model.OpaResultResponse{}, nil
	}

	return &response, nil
}
