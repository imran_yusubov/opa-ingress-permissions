package main

import (
	"github.com/jessevdk/go-flags"
	"github.com/joho/godotenv"
	"github.com/ms-opa-permission/client"
	"github.com/ms-opa-permission/config"
	"github.com/ms-opa-permission/handler"
	"github.com/ms-opa-permission/router"
	"github.com/ms-opa-permission/service"
	log "github.com/sirupsen/logrus"
)

func main() {
	var err error
	log.SetFormatter(&log.JSONFormatter{})

	_, err = flags.Parse(&config.Opts)
	if err != nil {
		panic(err)
	}

	initEnvVars()
	config.LoadConfig()

	r := router.New()
	v1 := r.Group(config.Conf.RootPath)

	opaClient := client.OpaClient{OpaRootPath: config.Conf.OpaClientURL}

	opaPermissionService := service.NewOpaPermissionService(&opaClient)

	h := handler.NewHandler(opaPermissionService)
	h.Register(v1)
	r.Logger.Fatal(r.Start(":" + config.Conf.Port))
}

func initEnvVars() {
	if godotenv.Load("profiles/default.env") != nil {
		log.Fatal("Error in loading environment variables from: profiles/default.env")
	} else {
		log.Info("Environment variables loaded from: profiles/default.env")
	}

	if config.Opts.Profile != "default" {
		profileFileName := "profiles/" + config.Opts.Profile + ".env"
		if godotenv.Overload(profileFileName) != nil {
			log.Fatal("Error in loading environment variables from: ", profileFileName)
		} else {
			log.Info("Environment variables overloaded from: ", profileFileName)
		}
	}
}
