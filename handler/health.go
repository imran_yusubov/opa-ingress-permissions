package handler

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// Health ...
func (h *Handler) Health(c echo.Context) error {
	return c.NoContent(http.StatusOK)
}
