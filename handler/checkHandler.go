package handler

import (
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"net/http"
)

// Check ...
func (h *Handler) Check(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	result, err := h.OpaPermissionService.Check(body)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, result)
}
