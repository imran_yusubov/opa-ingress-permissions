package handler

import "github.com/ms-opa-permission/service"

// Handler ...
type Handler struct {
	*service.OpaPermissionService
}

// NewHandler ...
func NewHandler(ops *service.OpaPermissionService) *Handler {
	return &Handler{ops}
}
