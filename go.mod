module github.com/ms-opa-permission

go 1.15

require (
	github.com/alexflint/go-arg v1.3.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/labstack/gommon v0.3.0
	github.com/sirupsen/logrus v1.7.0
)
